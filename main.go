package main

//导入gin

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"log"

	//"github.com/thinkerou/favicon"
	"net/http"
)

//自定义中间件 拦截器
func myHandler() gin.HandlerFunc {
	return func(context *gin.Context) {
		//通过自定义的中间件,设置的值在后续处理只要调用了这个中间件都可以拿到
		context.Set("usersession", "userid-1")
		context.Next() //通过了就继续下一步 放行
		//  context.Abort() //每通过就可以在这里阻止
	}
}

func main() {
	//创建一个服务
	router := gin.Default()
	//router.Use(favicon.New("./favicon.ico"))

	// 访问地址,处理我们的请求 Request Response
	router.GET("/hello", func(context *gin.Context) {
		context.JSON(200, gin.H{"msg": "hello,world"})
	})

	////Gin RestFul 十分简单的
	//router.POST("/user", func(context *gin.Context) {
	//	context.JSON(200,gin.H{"msg":"post"})
	//})
	//router.PUT("/user")
	//router.DELETE("/user")

	//##########返回静态文件###########################

	//加载静态页面
	router.LoadHTMLGlob("templates/*")
	//加载资源文件
	router.Static("/static", "./static")
	//router.LoadHTMLFiles("templates/index.html")//加载单个文件
	//响应一个页面给前端
	router.GET("/index", func(context *gin.Context) {
		//context.JSON() //返回json数据
		context.HTML(http.StatusOK, "index.html", gin.H{"msg": "这是go后台传递过来的数据"})
	})

	//#####################################

	//获取前端传递过来的参数 url?userid=xxx&username=张三

	router.GET("/user/info", myHandler(), func(context *gin.Context) {

		//取出中间件中的值

		usersession := context.MustGet("usersession").(string) //.(string)意思是强转

		log.Println("================>",usersession) //控制台打印输出

		username := context.Query("name") //接收前端传递过来的姓名
		context.JSON(http.StatusOK, gin.H{"username": username})
	})

	// resf 接收参数方式 http://localhost:8082/user/info/1/2
	router.GET("/user/info/:userid/:username", func(context *gin.Context) {
		username := context.Param("username") //接收前端传递过来的姓名  路由中放置参数只能用Param方式接收
		context.JSON(http.StatusOK, gin.H{"username": username})
	})

	//前端给后端传递一个json

	router.POST("/json", func(context *gin.Context) {
		//request.body
		data, _ := context.GetRawData()
		var m map[string]interface{}
		//包装为json数据 [] byte
		_ = json.Unmarshal(data, &m)
		context.JSON(http.StatusOK, gin.H{"data": m})

	})

	//接收表单传递过来的数据 index.html
	router.POST("/user/add", func(context *gin.Context) {
		username := context.PostForm("user_name")
		pwd := context.PostForm("pwd")
		context.JSON(http.StatusOK, gin.H{"user_name": username, "pwd": pwd})
	})

	// 路由
	router.GET("/test", func(context *gin.Context) {
		//重定向状态码是301
		context.Redirect(http.StatusMovedPermanently, "https://www.runoob.com/") //重定向
	})

	//404  NoRoute
	router.NoRoute(func(context *gin.Context) {
		context.HTML(http.StatusNotFound, "404.html", nil)
	})

	//路由组
	userGroup := router.Group("/group")
	{
		userGroup.GET("/add")
		userGroup.POST("/login")
	}
	order := router.Group("/order")
	{
		order.POST("create_order")
		order.GET("info")
	}

	//服务器端口(运行启动  go run main.go)
	_ = router.Run(":8082")
}
