# 设置基础镜像
FROM golang:1.18.3
ENV WORK_DIR /data/work/gin_study

# 将本地文件复制到容器中
COPY . $WORK_DIR

# 拷贝镜像源
COPY  docker/debian/debian11_source_list /etc/apt/
# 拷贝启动软件ssh
COPY docker/ssh/start_service.sh /bin/
# 拷贝守护进程任务
COPY docker/supervisor/ /etc/supervisor/conf.d

RUN cp /etc/apt/sources.list /etc/apt/sources.list.back \
   && chmod -R 777 /etc/apt/debian11_source_list \
   && cat  /etc/apt/debian11_source_list > /etc/apt/sources.list \
   && apt-get update \
   && apt-get install -y zip unzip vim wget net-tools supervisor \
   # 修改国内镜像源
   && go env -w GOPROXY=https://mirrors.aliyun.com/goproxy/ \
   #安装依赖
   && cd $WORK_DIR \
   && go mod tidy -compat=1.17 \
   && go mod download \
   && go mod vendor \
   # 安装热更新1.8.3版本可用
   && go get -u github.com/cosmtrek/air \
   # 配置supervisor日志文件位置
   && mkdir /etc/supervisor/log && chmod -R 777 /etc/supervisor/log \
   # 配置启动文件
   && chmod a+x  /bin/start_service.sh

# 构建应用(编译为可执行文件)
#RUN go build -o main . \

# 设置工作目录
WORKDIR $WORK_DIR

# 配置启动命令
#CMD ["/bin/start_service.sh"]
